package lesson6;

public class Chevrolet extends Car {

    private String engine = "CoolEngine";
    private String xenon;

    public Chevrolet(String name, String model, int speed) {
        super(name, model, speed);
    }

    public String getEngine() {
        return engine;
    }

    public String getXenon() {
        return xenon;
    }
}
