package lesson6;

public abstract class Car {
   public static final boolean IS_ALLOWED_IN_UKRAINIAN = true;
    protected String name;
    protected String model;
    protected int speed;


    public Car(String name, String model, int speed) {
        this.name = name;
        this.model = model;
        this.speed = speed;
    }



    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public int getSpeed() {
        return speed;
    }
}


