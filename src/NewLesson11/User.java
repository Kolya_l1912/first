package NewLesson11;

public class User implements Comparable<User>{
    private String firstName;
    private String lasName;
    private int age;

    public User(String firstName, String lasName, int age) {
        this.firstName = firstName;
        this.lasName = lasName;
        this.age = age;
    }
    public void showData(){
        System.out.println("First name = " + this.firstName);
        System.out.println("Last name = " + this.lasName);
        System.out.println("Age = " + this.age);
    }

    @Override
    public int compareTo(User o) {
        return new Integer(age).compareTo(new Integer(o.age));
    }
}
