package NewLesson11;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        User sara = new User("Sara","Johns", 23);
        User john = new User("John","Johns", 25);
        User michael = new User("Michael","Johns", 27);
        User denis = new User("Denis","Johns", 24);

        Map<String, User> userMap = new HashMap<>();
        userMap.put("Sara", sara);
        userMap.put("John", john);
        userMap.put("Denis", denis);

        List<User> users = new ArrayList<>();
        users.add(sara);
        users.add(john);

        List<User> users1 = new LinkedList<>();
        users1.add(michael);
        users1.add(denis);

//        Set<User> users2 = new TreeSet<User>();
//        users2.add(sara);
//        users2.add(sara);
//        users2.add(john);
//        users2.add(john);
//        users2.add(michael);
//        users2.add(denis);
//
//        showCollection(users2);
    }

    public static void showCollection(Collection<User> collection){
        System.out.println("Start..\n");
        for(User user: collection){
            user.showData();
            System.out.println("\n");
        }
        System.out.println("\n");
    }
}
