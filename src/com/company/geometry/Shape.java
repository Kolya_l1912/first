package com.company.geometry;

public abstract class Shape {
    protected double square;
    protected double perimeter;

    public abstract Double solvePerimeter();

    public abstract Double solveSquare();

    public void showParameters() {
        if (perimeter == 0) {
            System.out.println("Perimeter is not solved yet");
        }
        if (square == 0) {
            System.out.println("Square is not solved yet");
        }
        System.out.println("Perimeter = " + perimeter);
        System.out.println("Square = " + square);
    }
}
