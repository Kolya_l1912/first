package com.company.geometry;

public class Triangle extends Shape {
    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double sideA, double sideB, double sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    @Override

    public Double solvePerimeter() {
        super.perimeter =  sideA + sideB + sideC;
        return super.perimeter;
    }

    @Override
    public Double solveSquare() {
        if (super.perimeter == 0){
            solvePerimeter();
        }
        double halfPerimeter = super.perimeter/2;
        super.square = Math.sqrt(halfPerimeter * (halfPerimeter - sideA) * (halfPerimeter - sideB) * (halfPerimeter - sideC));
        return super.square;
    }
}
