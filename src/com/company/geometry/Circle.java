package com.company.geometry;

public class Circle extends Shape {

    private double radius;

    public Circle(double radius) {
        this.radius = radius;
    }

    @Override
    public Double solvePerimeter() {
         super.perimeter =  2 * Math.PI * radius;
        return super.perimeter;
    }

    @Override
    public Double solveSquare() {
        super.square =  Math.PI * Math.pow(radius, 2);
        return super.square;
    }
}
