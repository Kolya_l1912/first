package com.company.geometry;

public class Rectangle extends Shape {
    private double width;
    private double height;

    public Rectangle(double width, double height) {
        this.width = width;
        this.height = height;
    }

    @Override
    public Double solvePerimeter() {
         super.perimeter =  2 * (width + height) ;
        return super.perimeter;
    }

    @Override
    public Double solveSquare() {
        super.square = width * height;
        return super.square;
    }
}
