package com.company;

public class lesson {
    public static void main(String[] args) {
        int condition = 3;
        String name = "kolia";
        System.out.println("=====Loop for====");
        loopFor(condition, name);
        System.out.println("=====Loop while");
        loopWhile(condition, name);
        System.out.println("=====Loop do while");
        loopDoWhile(condition, name);


    }

    private static void loopFor(int conditionToStopLoop, String name) {
        for (int b = 0; b < conditionToStopLoop; b++) {
            System.out.println(name);
        }
    }

    private static void loopWhile(int conditionToStopLoop, String name) {
        while (conditionToStopLoop > 0) {
            System.out.println(name);
            conditionToStopLoop--;
        }
    }

    private static void loopDoWhile(int conditionToStopLoop, String name) {
        do {
            System.out.println(name);
            conditionToStopLoop--;
        } while (conditionToStopLoop > 0);

    }

    private static void chooseLoop(int index, String text) {
        switch (index) {
            case 226:
                System.out.println();
            case 145:
                System.out.println();
            case 789:
                System.out.println();
            default:
            case 456:
                System.out.println();

        }
    }


}

