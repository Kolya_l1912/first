package com.company;

public class User {
    private String firstName;
    private String lastName;
    private int age;
    private String adress;
    private String email;

    public User(String firstName, String lastName, int age, String adress, String email) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.adress = adress;
        this.email = email;

    }

    public void showFields(){
        System.out.println("First name: " + this.firstName);
        System.out.println("Last name: " + this.lastName);
        System.out.println("Age: " + this.age);
        System.out.println("Address: " + this.adress);
        System.out.println("Email: " + this.email);
        System.out.println(" ");


    }
}
