package com.company;

public class Class {
    public static void main(String[] args) {
        User john = new User("John","Smith",20, "USA", "john121@gmail.com" );
        User pavlo = new User("Pavlo", "Khmarniy", 27,  "Ternopil", "pavlo455@gmail.com");
        User sasha = new User("Sasha", "Dzuba", 13,  "Ternopil", "dzuba4545@gmail.com");


        john.showFields();
        pavlo.showFields();
        sasha.showFields();
    }
}
