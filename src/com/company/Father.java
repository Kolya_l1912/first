package com.company;

public class Father implements Name{
    private String character = "character";
    protected String skin = "white";
    protected String eye = "brown";

    public Father (String character){
        this.character=character;
    }

    public void setCharacter(String character){
        this.character=character;
    }

    public String getCharacter(){
        return character;
    }

    @Override
    public void showData() {

    }
}


