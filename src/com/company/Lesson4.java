package com.company;

import com.company.geometry.Circle;
import com.company.geometry.Triangle;

public class Lesson4 {
    public static void main(String[] args) {
        Triangle triangle = new Triangle(2,4,5);
        triangle.solvePerimeter();
        triangle.solveSquare();
        triangle.showParameters();

        Circle circle = new Circle(6);
        circle.solvePerimeter();
        circle.solveSquare();
        circle.showParameters();
    }

}
