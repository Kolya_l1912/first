package com.company;

public class Project {

    private static final int ARRAY_SIZE = 6;

    private static final int FOOTBALL = 1;
    public static final int CHESS = 2;
    public static final int PING_PONG = 3;

    public static void main(String[] args) {
        int[] numbers = new int[ARRAY_SIZE];

        for (int i = 0; i < ARRAY_SIZE; i++) {
            numbers[i] = i;
        }

        for (int i = 0; i < numbers.length; i++) {
            System.out.println(numbers[i]);
        }
        String playerName = "Vasil";
        int playerAge = 12;
        int playerEnergy = 28;

        String playerName1 = "Sasha";
        int playerAge1 = 20;
        int playerEnergy1 = 40;

        String playerName2 = "Vital";
        int playerAge2 = 60;
        int playerEnergy2 = 20;

        startGame(playerName, playerAge, playerEnergy, FOOTBALL);
        startGame(playerName1, playerAge1, playerEnergy1, CHESS);
        startGame(playerName2, playerAge2, playerEnergy2, PING_PONG);
    }

    private static void startGame(String name, int age, int energy, int gameAddress) {
        switch (gameAddress) {
            case FOOTBALL:
                football(name, age, energy);
                break;
            case CHESS:
                chess(name, age, energy);
                break;
            case PING_PONG:
                pingPong(name, age, energy);
                break;
        }
    }

    public static void football(String name, int age, int energy) {
        System.out.println("Player -> " + name + " start football");
        if (age > 10 && age < 45) {
            System.out.println("Player can play game!");
        } else {
            System.out.println("Player can't play game,because age not allowed!");
            return;
        }
        int[] goals = new int[energy];

        for(int i = 0; i < goals.length; i++){
            int hit = (int) (Math.random()*9);
            if(hit > 3 && hit <6){
                goals[i] = 1;
            }else{
                goals[i] = 0;
            }
        }
        for(int i = 0;i < goals.length; i++){
            if (goals[i] ==1){
                System.out.println("Player histed goal!");
            }else{
                System.out.println("Player fall");
            }
        }
    }

    public static void chess(String name, int age, int energy) {
        System.out.println("Player -> " + name + " start chess");

    }

    public static void pingPong(String name, int age, int energy) {
        System.out.println("Player -> " + name + " start pingPong");
        if (age > 10 && age < 45) {
            System.out.println("Player can play game!");
        } else {
            System.out.println("Player can't play game,because age not allowed!");
        }
    }
}
