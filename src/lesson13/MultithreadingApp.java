package lesson13;

import lesson14.Clock;
import lesson14.CustomThread;

public class MultithreadingApp {
    public static void main(String[] args) {
        System.out.println("Main thread is started!");
        Clock clock = new Clock();
        CustomThread thread1 = new CustomThread(clock, "Thread 1",400);
        CustomThread thread2 = new CustomThread(clock, "Thread 2",400);



//        Counter counter1 = new Counter(" Child 1", 400);
//        Counter counter2 = new Counter(" Child 2", 700);
//        Counter counter3 = new Counter(" Child 3", 1000);
        System.out.println("Main thread is finished!");
    }
}
