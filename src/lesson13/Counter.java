package lesson13;

public class Counter implements Runnable {
    private int sleep;
    private Thread thread;

    public Counter(String threadName ,int sleep) {
        this.sleep = sleep;
        thread = new Thread(this);
        thread.setName(threadName);
        thread.start();
    }

    @Override
    public void run() {
        System.out.println(thread.getName() + "started!");
        for (int i = 0; i < 10; i++) {
            try {
                System.out.println("Thread" + thread.getName() + ", count = " +i);
                Thread.sleep(sleep);
            } catch (InterruptedException e) {
                System.out.println(thread.getName() + "interrupted");
            }
        }
        System.out.println("Thread finished");
}
}
