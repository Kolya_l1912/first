package lesson14.Lambdas;

public class CountableImpl implements Countable {
    @Override
    public int count(int number) {
        int sum = 0;
        for (int i = 0; i < number; i ++){
            sum++;
        }
        return sum;
    }
}
