package lesson14.Lambdas;


public class LambdaApp {
    public static void main(String[] args) {
        Countable countable = new CountableImpl();
        int sum1 = countable.count(10);
        int sum2 = countable.count(15);

        Countable countable1 = new Countable(){

            @Override
            public int count(int number) {
                int sum1 = 0;
                for(int i = 0; i< number; i++){
                    sum1++;
                }
                return sum1;
            }
        };



        Countable countable2 = (n) -> {
            int sum = 0;
            for (int i = 0; i < n; i++) {
                sum++;
            }
            return sum;
        };
    }
}
