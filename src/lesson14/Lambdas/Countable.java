package lesson14.Lambdas;

public interface Countable {
    int count(int number);
}
