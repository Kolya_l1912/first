package lesson9.button;

public class AppCompatActivity implements Activity {
    private Button button;

    @Override
    public void onCreate() {
        button = new Button(10,25);
        button.setEventListener(new ButtonClickListener()) ;

        button.click();
        button.click();
        button.click();
    }

}
