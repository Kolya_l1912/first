package lesson9.button;

public abstract class View {
    private EventListener eventListener;

    public void setEventListener(EventListener eventListener) {
        this.eventListener = eventListener;
    }
    public void click() {
        eventListener.onClick();
    }
}

