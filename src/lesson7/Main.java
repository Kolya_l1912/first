package lesson7;

import lesson7.model.Skin;
import lesson7.model.SkinRole;
import lesson7.model.Weapon;


public class Main {
    public static void main(String[] args) {
        Skin bot = new Skin(100, new Weapon(20, "Sg"),"Dog", SkinRole.BOT);
        Skin gamer = new Skin(100, new Weapon(25, "Ak-47"),"Bubelby", SkinRole.GAMER);

        GameLogic gameLogic = new GameLogic();


        gameLogic.startGame(gamer, bot);

    }
    }