package lesson7;

import lesson7.model.Skin;
import lesson7.model.Weapon;

import java.util.Scanner;

public class GameLogic {

    private Skin gamer;

    private Skin bot;

    public void startGame(Skin gamer, Skin bot) {
        this.gamer = gamer;
        this.bot = bot;

        for (int i = 0; i < 5; i++) {
            gamerShoot();
            if(!isSkinHasHealth(bot)) break;
            botShoot();
            if(!isSkinHasHealth(gamer)) break;
        }
        gameOver();
    }

    private void gamerShoot() {
        System.out.println("Player enter shoot");
        Scanner scanner = new Scanner(System.in);
        int shoot = scanner.nextInt();

        shootingSkinRange(bot, gamer.getWeapon(), shoot);
    }

    private void botShoot() {
        System.out.println("Bot is playing...");
        int shoot = (int) (Math.random() * 14);
        System.out.println("Bot shoot " + shoot);

        shootingSkinRange(gamer, bot.getWeapon(), shoot);
    }

    private boolean isSkinHasHealth(Skin skin) {
        return skin.getHealth() > 0;
    }

    private void shootingSkinRange(Skin skin, Weapon enemyWeapon, int shoot) {
        if (shoot > 5 && shoot < 9) {
            skin.setHealth(skin.getHealth() - enemyWeapon.getDamage());

        }
    }

    private void gameOver() {
        if (gamer.getHealth() > bot.getHealth()) {
            System.out.println("Gamer " + gamer.getName() + " is winner");
        } else {
            System.out.println("Bot " + bot.getName() + " is winner");
        }
    }
}
