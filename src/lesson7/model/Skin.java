package lesson7.model;

public class Skin {
    private int health;
    private Weapon weapon;
    private String name;
    private SkinRole skinRole;

    public  Skin(){

    }

    public Skin(int health, Weapon weapon, String name, SkinRole skinRole) {
        this.health = health;
        this.weapon = weapon;
        this.name = name;
        this.skinRole = skinRole;
    }

    public int getHealth() {
        return health;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public String getName() {
        return name;
    }

    public SkinRole getSkinRole() {
        return skinRole;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSkinRole(SkinRole skinRole) {
        this.skinRole = skinRole;
    }
}
