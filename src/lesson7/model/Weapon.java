package lesson7.model;

public class Weapon {
    private int damage;
    private String weaponType;

    public Weapon(int damage, String weaponType) {
        this.damage = damage;
        this.weaponType = weaponType;
    }

    public int getDamage() {
        return damage;
    }

    public String getWeaponType() {
        return weaponType;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setWeaponType(String weaponType) {
        this.weaponType = weaponType;
    }
}
