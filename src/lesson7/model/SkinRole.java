package lesson7.model;

public enum SkinRole {
    GAMER,
    ADMIN,
    BOT
}
