package NewYear.Util;

public class MyCollection<T> {

    private Object[] masOfTypeObjects;

    public MyCollection() {
        masOfTypeObjects = new Object[0];
    }

    public void setMasOfTypeObjects(Object[] masOfTypeObjects) {
        this.masOfTypeObjects = masOfTypeObjects;
    }

    public Object[] getMasOfTypeObjects() {
        return masOfTypeObjects;
    }

    public void add(T object){
        Object[] tempMas = new Object[masOfTypeObjects.length];
        for(int i=0; i<masOfTypeObjects.length;i++){
            tempMas[i]=masOfTypeObjects[i];
        }
        int tempCount = masOfTypeObjects.length;
        tempCount++;
        masOfTypeObjects = new Object[tempCount];
        for (int i=0; i<tempCount;i++){
            if(i==tempCount - 1){
                masOfTypeObjects[i]= object;
            }else{
                masOfTypeObjects[i]= tempMas[i];
            }
        }
    }
    public T get(int index){
        if(index>masOfTypeObjects.length){
            return null;
        }
        return (T)masOfTypeObjects[index];
    }
}
