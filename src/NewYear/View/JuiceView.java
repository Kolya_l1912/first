package NewYear.View;

public interface JuiceView {

    String getDataFromConsole();

    void showData (String message);

    void showSeparator();
    void showStartingFabrickWork();

    int getVariationsOfFabricWork();
}
