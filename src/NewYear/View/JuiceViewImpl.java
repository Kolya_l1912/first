package NewYear.View;

import NewYear.Presenter.ViewPresenter;

import java.util.Scanner;

public class JuiceViewImpl implements JuiceView {

    public JuiceViewImpl() {
        new ViewPresenter(this);
    }

    @Override
    public String getDataFromConsole() {
        return new Scanner(System.in).next();
    }

    @Override
    public void showData(String message) {
        System.out.println(message);
    }

    @Override
    public void showSeparator() {
        System.out.println("---------------------------------------");
    }

    public void showStartingFabrickWork(){
        System.out.println("Welcome to fabric");
    }

    @Override
    public int getVariationsOfFabricWork(){
        System.out.println("1 - show all juices\n" + "2 - show juice by params\n" + "3 - delete juice by id\n"+ "4 - create juice\n"+ "5 - end fabric work");
        return new Scanner(System.in).nextInt();
    }
}
