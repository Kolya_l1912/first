package NewYear.Presenter;

import NewYear.Model.Juice;
import NewYear.Model.Type.JuiceType;
import NewYear.View.JuiceView;

import java.util.ArrayList;
import java.util.List;

public class ViewPresenter {
    private JuiceView juiceView;

    private List<Juice> listOfJuice = new ArrayList<>();

    public ViewPresenter(JuiceView juiceView) {
        this.juiceView = juiceView;
        setUpDatabase();
        startFabrikWork();
    }

    private void setUpDatabase() {
        listOfJuice.add(new Juice(1, "Sadochok", 1, JuiceType.APPLE));
        listOfJuice.add(new Juice(2, "Sandora", 3, JuiceType.APPLE));
        listOfJuice.add(new Juice(3, "Sadochok", 2, JuiceType.ORANGE));
        listOfJuice.add(new Juice(4, "Nash Sic", 2, JuiceType.BANANA));
    }

    public void startFabrikWork() {
        juiceView.showStartingFabrickWork();
        boolean isFabrikWorking = true;
        do {
            int variationOfWork = juiceView.getVariationsOfFabricWork();
            switch (variationOfWork){
                case 1:
                    showAllJuices();
                    break;
                case 2:
                    showJuiceByParams();
                    break;
                case 3:
                    juiceView.showData("Enter please id");
                    deleteJuiceById(Integer.parseInt(juiceView.getDataFromConsole()));
                    break;
                case 4:
                    creatJuice();
                    break;
                case 5:
                    isFabrikWorking = false;
                    break;
            }

        }while(isFabrikWorking);
    }

    private JuiceType applyJuiceType(int index) {
        switch (index) {
            case 1:
                return JuiceType.APPLE;
            case 2:
                return JuiceType.ORANGE;
            case 3:
                return JuiceType.BANANA;

                default:return null;
        }
    }

    public void creatJuice() {
        Juice newJuice = new Juice();
        juiceView.showData("Enter juice id");
        int juiceId = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setId(juiceId);
        juiceView.showData("Enter juice volume");
        int volume = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setVolumes(volume);
        juiceView.showData("Enter juice name");
        String name = juiceView.getDataFromConsole();
        newJuice.setName(name);
        juiceView.showData("Enter Juice type: 1-Apple, 2-orange, 3-Banana");
        int typeIndex = Integer.parseInt(juiceView.getDataFromConsole());
        newJuice.setJuiceType(applyJuiceType(typeIndex));
    }

    public void showJuiceByParams() {
        Juice juiceExampleObject = new Juice();

        juiceView.showData("Enter juice id");
        int juiceId = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setId(juiceId);
        juiceView.showData("Enter juice volume");
        int volume = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setVolumes(volume);
        juiceView.showData("Enter juice name");
        String name = juiceView.getDataFromConsole();
        juiceExampleObject.setName(name);
        juiceView.showData("Enter Juice type: 1-Apple, 2-orange, 3-Banana");
        int typeIndex = Integer.parseInt(juiceView.getDataFromConsole());
        juiceExampleObject.setJuiceType(applyJuiceType(typeIndex));


        for (Juice item : listOfJuice) {
            if (item.getId() == juiceExampleObject.getId()) {

            } else if (item.getName() == juiceExampleObject.getName()) {

            } else if (item.getVolumes() == juiceExampleObject.getVolumes()) {

            }
        }

    }

    public void deleteJuiceById(int id) {
        for (Juice item : listOfJuice) {
            if (item.getId() == id) {
                listOfJuice.remove(item);
                juiceView.showData("Item " + item.getId() + " has been deleted");
            }
        }
    }

    public void showAllJuices() {
        for (Juice item : listOfJuice) {
            showJuice(item);
        }
    }

    private void showJuice(Juice juice) {
        juiceView.showData("Id ->" + juice.getId());
        juiceView.showData("Name ->" + juice.getName());
        juiceView.showData("Volume ->" + juice.getVolumes());
        juiceView.showData("Type of juice ->" + juice.getJuiceType());
        juiceView.showSeparator();
    }
}
