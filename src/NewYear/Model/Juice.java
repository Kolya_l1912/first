package NewYear.Model;

import NewYear.Model.Type.JuiceType;

public class Juice {
    private int id;
    private String name;
    private int volumes;
    private JuiceType juiceType;

    public Juice() {
    }

    public Juice(int id, String name, int volumes, JuiceType juiceType) {
        this.id = id;
        this.name = name;
        this.volumes = volumes;
        this.juiceType = juiceType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getVolumes() {
        return volumes;
    }

    public void setVolumes(int volumes) {
        this.volumes = volumes;
    }

    public JuiceType getJuiceType() {
        return juiceType;
    }

    public void setJuiceType(JuiceType juiceType) {
        this.juiceType = juiceType;
    }
}
