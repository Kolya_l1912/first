package lesson5;

public class Main2 {
    public static void main(String[] args) {
        String alfabet = "Hello World";
        String alfabetChar =alfabet.substring(2,5);
        System.out.println(alfabetChar);

        String alfabetChar2 =alfabet.substring(6);
        System.out.println(alfabetChar2);

        int alfabetInt = alfabet.indexOf("W");
        System.out.println(alfabetInt);

        boolean alfabetBoolean = alfabet.equals("Hello World");
        System.out.println(alfabetBoolean);

        String alfabetStr = alfabet.replaceAll("Hello World", "dlroW olleH");
        System.out.println(alfabetStr);
    }
}
