package lesson5;

public interface Math {
    int plus(int a,int b);
    int minus(int a,int b);
    int multiply(int a,int b);
}
