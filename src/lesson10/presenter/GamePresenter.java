package lesson10.presenter;

import lesson10.model.Skin;
import lesson10.model.Weapon;
import lesson10.model.type.SkinRole;
import lesson10.model.type.WeaponRole;
import lesson10.view.GameView;
import lesson7.model.WeaponType;

public class GamePresenter {
    private final int SKIN_HEALTH = 100;

    private GameView gameView;
    private Weapon[] weapons = new Weapon[4];

    Skin user;
    Skin robot;

    public GamePresenter(GameView gameView) {
        this.gameView = gameView;
    }

    public void startGame() {
        createWeapon();
        createUser();
        createBot();


        int whoIsShooting = (int) (Math.random() * 3);

        boolean isGamePlaying = true;
        do {
            if (whoIsShooting >= 1) {
                skinShoot(user, robot);
                whoIsShooting = 0;
                gameView.setSeparator();
                skinSate(robot);
                isGamePlaying = isSkinHasHealth(robot);
            } else {
                skinShoot(robot, user);
                whoIsShooting = 1;
                gameView.setSeparator();
                skinSate(user);
                isGamePlaying = isSkinHasHealth(user);
            }

        } while (isGamePlaying);

        gameView.setSeparator();
        if (user.getHealth() > robot.getHealth()) {
            gameView.outPutText(user.getName() + " is Winner!");
        }else{
            gameView.outPutText(robot.getName()+ " kill " + user.getName());
        }
    }

    private void createUser() {
        gameView.setSeparator();
        gameView.outPutText("Set user name:");
        String userName = gameView.getDataFromView();
        gameView.outPutText("Set weapon:");
        int weaponIndex = Integer.parseInt(gameView.getDataFromView());
        Weapon weapon = chooseWeapon(weaponIndex);
        user = new Skin(userName, weapon, SKIN_HEALTH, SkinRole.USER);
        gameView.outPutText("User -> " + userName + " crated!");
    }

    private void createBot() {
        gameView.setSeparator();
        Weapon weapon = chooseWeapon((int) (Math.random() * weapons.length - 1));
        robot = new Skin("Robot ", weapon, SKIN_HEALTH, SkinRole.BOT);
        gameView.outPutText("Bot ->" + robot.getName() + " created!");
    }

    private void skinSate(Skin skin) {
        gameView.outPutText("Skin -> " + skin.getName() + "\n" + "health ->" + user.getHealth() + "\n" + "role ->" + skin.getSkinRole());
    }


    private void skinShoot(Skin hunter, Skin prey) {
        gameView.setSeparator();
        gameView.outPutText(hunter.getName() + " shooting " + prey.getName());
        if (isHunterHitPrey(hunter)) {
            prey.setHealth(prey.getHealth() - hunter.getWeapon().getDamage());
        } else {
            gameView.outPutText(hunter.getName() + " miss shoot");
        }
    }

    private boolean isHunterHitPrey(Skin hunter) {
        boolean result = false;
        int shoot;
        gameView.outPutText(hunter.getName() + " make shoot: ");
        if (hunter.getSkinRole() == SkinRole.USER) {
            String temp = gameView.getDataFromView();
            shoot = Integer.parseInt(temp);
        } else {
            shoot = (int) (Math.random() * 14);
        }
        gameView.outPutText(" shoot is ->" + shoot);

        if (shoot > 4 && shoot < 10) {
            result = true;
        }
        return result;
    }

    private void createWeapon() {
        weapons[0] = new Weapon("Katana", 30, WeaponRole.KNIFE);
        weapons[1] = new Weapon("AK - 47", 40, WeaponRole.GUN);
        weapons[2] = new Weapon("M16", 42, WeaponRole.GUN);
        weapons[3] = new Weapon("Knife", 10, WeaponRole.KNIFE);
    }

    private Weapon chooseWeapon(int weaponIndex) {
        if (weaponIndex >= weapons.length) {
            return weapons[weapons.length - 1];
        }
        return weapons[weaponIndex];
    }

    private boolean isSkinHasHealth(Skin skin) {
        return skin.getHealth() > 0;
    }
}
