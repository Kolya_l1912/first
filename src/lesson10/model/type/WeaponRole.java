package lesson10.model.type;

public enum WeaponRole {
    KNIFE,
    GUN
}
