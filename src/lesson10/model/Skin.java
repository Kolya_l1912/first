package lesson10.model;

import lesson10.model.type.SkinRole;

public class Skin {
    private String name;

    private Weapon weapon;

    private int health;

    private SkinRole skinRole;

    public Skin(String name, Weapon weapon, int health, SkinRole skinRole) {
        this.name = name;
        this.weapon = weapon;
        this.health = health;
        this.skinRole = skinRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Weapon getWeapon() {
        return weapon;
    }

    public void setWeapon(Weapon weapon) {
        this.weapon = weapon;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public SkinRole getSkinRole() {
        return skinRole;
    }

    public void setSkinRole(SkinRole skinRole) {
        this.skinRole = skinRole;
    }
}
