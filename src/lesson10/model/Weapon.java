package lesson10.model;

import lesson10.model.type.WeaponRole;

public class Weapon {
    private String name;
     private int Damage;

    private WeaponRole weaponRole;



    public Weapon(String name, int getDamage, WeaponRole weaponRole) {
        this.name = name;
        this.Damage = getDamage;
        this.weaponRole = weaponRole;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDamage() {
        return Damage;
    }

    public void setGetDamage(int getDamage) {
        this.Damage = getDamage;
    }

    public WeaponRole getWeaponType() {
        return weaponRole;
    }

    public void setWeaponType(WeaponRole weaponType) {
        this.weaponRole = weaponType;
    }
}
