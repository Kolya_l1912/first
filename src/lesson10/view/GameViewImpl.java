package lesson10.view;

import lesson10.presenter.GamePresenter;

import java.util.Scanner;

public class GameViewImpl implements GameView {

    public GameViewImpl(){
        new GamePresenter(this).startGame();
    }

    @Override
    public void outPutText(String outPutText) {
        System.out.println(outPutText);
    }

    @Override
    public String getDataFromView() {
        Scanner scanner = new Scanner(System.in);
        return scanner.next();
    }

    @Override
    public void setSeparator() {
        System.out.println("---------------------------");
    }
}
