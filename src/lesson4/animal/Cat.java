package lesson4.animal;

public class Cat extends Animal{

    public Cat(){

    }

    public Cat(String name){
        super(name);
    }
    @Override
    public String say() {
        return "Meow";
    }

    @Override
    public void eat() {
        System.out.println("I drink milk");
    }
}
