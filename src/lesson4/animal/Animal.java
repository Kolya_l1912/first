package lesson4.animal;

abstract public class Animal {
    protected String name;

    public Animal(){

    }

    public Animal(String name) {
        this.name = name;
    }

    public abstract String say();
    public abstract void eat();

    public void myNameIs(){
        if (name == null || name.isEmpty() ){
            System.out.println("I don't have any name");
        }else{
            System.out.println("My name is " + name);
        }
    }

}
