package lesson4.animal;

public class Dog extends Animal {
    public Dog() {

    }

    public Dog(String name) {
        super(name);
    }

    @Override
    public String say() {
        return "Woof";
    }

    @Override
    public void eat() {
        System.out.println("I eat bones");
    }
}
