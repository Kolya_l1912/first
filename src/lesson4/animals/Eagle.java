package lesson4.animals;

public class Eagle extends Animal {
    public Eagle(String name, int countOfLegs, Skin skin, Security security, AnimalType animalType) {
        super(name, countOfLegs, skin, security, animalType);
    }

    @Override
    public void animalVoice() {
        System.out.println("Eagle is screaming");
    }

    public void eatRabbits(){
        System.out.println("Eating rabbits");
    }
}
