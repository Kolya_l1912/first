package lesson4.animals;

public class Dog extends Animal{
    public Dog(String name, int countOfLegs, Skin skin, Security security, AnimalType animalType) {
        super(name, countOfLegs, skin, security, animalType);
    }

    @Override
    public void animalVoice() {
        System.out.println("Dog is barging!!!");
    }

    public void getShoes(){
        System.out.println("Dog get for us shoes!!!");
    }
}
