package lesson4.animals;

public class Animal {
    protected String name;
    protected int countOfLegs;
    protected Skin skin;
    protected Security security;
    protected AnimalType animalType;

    public Animal(String name, int countOfLegs, Skin skin, Security security, AnimalType animalType) {
        this.name = name;
        this.countOfLegs = countOfLegs;
        this.skin = skin;
        this.security = security;
        this.animalType = animalType;
    }

    public void  animalVoice(){}

    public String getName() {
        return name;
    }

    public int getCountOfLegs() {
        return countOfLegs;
    }

    public Skin getSkin() {
        return skin;
    }

    public Security getSecurity() {
        return security;
    }

    public AnimalType getAnimalType() {
        return animalType;
    }
}
