package lesson4;

import lesson4.animals.*;

public class Main {
    public static void main(String[] args) {
        Animal animal = new Animal("Animal",4, Skin.SCALES, Security.HORNS, AnimalType.CARNIVORE);
        Dog dog =  new Dog("Nika", 4, Skin.WOll, Security.IKE, AnimalType.MAMAL);
        Eagle eagle = new Eagle("Bob",2, Skin.FEATHER, Security.CLAWS, AnimalType.PREDATOR);

        System.out.println(dog.getName());
        dog.animalVoice();
        dog.getShoes();

        System.out.println(eagle.getName());
        eagle.animalVoice();
        eagle.eatRabbits();

        System.out.println(dog.getAnimalType() == AnimalType.MAMAL);
    }

}
