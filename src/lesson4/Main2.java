package lesson4;

import lesson4.animal.Animal;
import lesson4.animal.Cat;
import lesson4.animal.Dog;

public class Main2 {
    public static void main(String[] args) {
        Animal cat = new Cat("Murka");
        Animal dog = new Dog();


        Animal[] animals = new Animal[2];
        animals[0] = cat;
        animals[1] = dog;

        for (int i = 0; i < animals.length; i++){
            animals[i].myNameIs();
            animals[i].eat();

            System.out.println(animals[i].say());
        }
    }
}
