package lesson8.dicision;

public class Decision {
    private static Status decide(Client client){
        boolean hasCredit = client.isHasCredit();
        if(!hasCredit){
            return Status.APPROVE;
        }else{
            return Status.CENCEL;
        }
    }

    public static void showDecisionForClient(Client client){
        Status decisionStatus = decide(client);
        System.out.println("Decision result for client " + client.getFirstName() + " " + client.getFirstName() + " = " + decisionStatus);
    }
}
