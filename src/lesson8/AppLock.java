package lesson8;

import lesson8.lock.*;

import java.util.Scanner;

public class AppLock {
    private Scanner scanner;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ElectronicalKey electronicalKey = new ElectronicalKey();
        ElectronicalLock electronicalLock = new ElectronicalLock(electronicalKey);

        electronicalLock.unlock();
        System.out.println("Enter code!");
        String code = scanner.next();
        electronicalLock.open(code);
    }

    public static void lockManipulation(Lock lock, Key key) {
        lock.unlock();
        lock.open(key);
        lock.lock();
        lock.close(key);
        lock.unlock();
        lock.close(key);
        lock.lock();
    }

    public static void electrinicalLockManipulation(ElectronicalLock lock) {
        lock.unlock();
        lock.lock();
    }
}
