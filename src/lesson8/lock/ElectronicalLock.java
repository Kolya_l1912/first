package lesson8.lock;

public class ElectronicalLock {
    private boolean isLocked;
    private boolean isClosed;

    private ElectronicalKey key;

    public ElectronicalLock(ElectronicalKey key){
        isLocked = true;
        isClosed = true;
         this.key = key;
    }

    public ElectronicalKey getKey() {
        return key;
    }

    public void lock() {
        isLocked = true;
    }


    public void unlock() {
        isLocked = false;
    }


    public void open(String code) {
        if(isClosed && isLocked){
            System.out.println("You must unlock before opening!");
        }else if (!isClosed){
            System.out.println("Already opened");
        }else if(isClosed && !isLocked){

            System.out.println("Electronical lock is opened!");
        }
    }

    public void close(String code) {
        if(!key.isCodeAccepted(code)){
            System.out.println("You entered wrong code!");
            return;
        }
        if(isClosed && isLocked || isClosed && !isLocked){
            System.out.println("Already closed!");
        }else if(!isClosed && isLocked){
            System.out.println("You must unlock before closing!");
        }else if(!isClosed && !isLocked){
            System.out.println("Electronical lock is closed");
        }
    }
}
