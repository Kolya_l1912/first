package lesson8.lock;

public class MechanicalKey implements Key {
    private boolean isExist;

    public MechanicalKey() {
        isExist = true;
    }

    public void setExist(boolean exist) {
        isExist = exist;
    }


    @Override
    public boolean isExist() {
        return isExist;
    }


    @Override
    public boolean isCodeAccepted(String code) {
        return false;
    }
}
