package lesson8.lock;

public interface Lock {
    void lock();

    void unlock();

    void open(Key key);

    void close(Key key);
}
