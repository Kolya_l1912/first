package lesson8.lock;

public interface Key {
    boolean isExist();
    void setExist(boolean exist);
    boolean isCodeAccepted(String code);
}
