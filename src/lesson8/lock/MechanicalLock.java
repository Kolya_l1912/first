package lesson8.lock;



public class MechanicalLock implements Lock {
    private boolean isLocked;
    private boolean isClosed;

    private Key key;

    public MechanicalLock(){
        isLocked = true;
        isClosed = true;
    }

    @Override
    public void lock() {
      isLocked = true;
        System.out.println("Mechanical lock is locked!");
    }

    @Override
    public void unlock() {
        isLocked = false;
        System.out.println("Mechanical lock is unlocked!");
    }

    @Override
    public void open(Key key) {
        if(!key.isExist()){
            System.out.println("You forgot put key!");
            return;
        }
        if(isClosed && isLocked){
            System.out.println("You must unlock before opening!");
        }else if (!isClosed){
            System.out.println("Already opened");
        }else if(isClosed && !isLocked){
            isClosed = false;
            System.out.println("Mechanical lock is opened!");
        }
    }

    @Override
    public void close(Key key) {
        if(!key.isExist()){
            System.out.println("You forgot put key!");
            return;
        }
    if(isClosed && isLocked || isClosed && !isLocked){
        System.out.println("Already closed!");
    }else if(!isClosed && isLocked){
            System.out.println("You must unlock before closing!");
        }else if(!isClosed && !isLocked){
        isClosed = true;
        System.out.println("Mechanical lock is closed");
    }
    }
}
