package lesson12;

public class User {
   private int id;
   private String name;

    public User() {
    }

    public User(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public int hashCode() {
        return id+name.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if(obj == null){
            return  false;
        }
        if (obj instanceof User){
            return ((User)obj).id== this.id && ((User)obj).name.equals(this.name);
        }else {
            return false;
        }
    }

    @Override
    public String toString() {
        return "{\"id\":"+id+"," +"\"name\":\""+name+"\"}";
    }
}
