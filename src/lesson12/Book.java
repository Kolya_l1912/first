package lesson12;

import lesson12.type.TypBook;

public class Book {
    private int id;
    private String title;
    private TypBook typBook;

    public Book(int id, String title, TypBook typBook) {
        this.id = id;
        this.title = title;
        this.typBook = typBook;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public TypBook getTypBook() {
        return typBook;
    }

    public void setTypBook(TypBook typBook) {
        this.typBook = typBook;
    }
}
