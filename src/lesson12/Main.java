package lesson12;

import java.util.*;

public class Main {
    public static void main(String[] args) {
        Set<User> userSet = new HashSet<>();
        userSet.add(new User(1,"Ulian"));
        userSet.add(new User(2,"Andriy"));
        userSet.add(new User(2,"Andriy"));
        userSet.add(new User(3,"Sasha"));


        for(User user: userSet){
            System.out.println(user.getId());
            System.out.println(user.getName());
            System.out.println("--------------------------");

        }

//        List<String> nameList = new ArrayList<>();
//        nameList.add("7");
//        nameList.add("6");
//        nameList.add(null);
//        nameList.add("4");
//        nameList.add("54");
//        for (String item : nameList) {
//
//
//            try {
//                System.out.println(item.substring(1));
//            } catch (Throwable e) {
//                System.out.println(e.getMessage());
//            }
//
//        }
//
   }
}
